import sys
from PySide2.QtWidgets import QApplication,QMainWindow,QDockWidget, QLabel
from PySide2.QtCore import Qt




from node_editor.base_objects.node import Node
from node_editor.base_objects.node_editor import node_editor
from node_editor.base_objects.socket_type import *
from node_editor.widgets.properties import *

from node_editor.nodes import math_nodes, basic_nodes, opencv_nodes
from node_editor.utils import *


if __name__ == '__main__':
    app = QApplication(sys.argv)

    window = QMainWindow()
    node_editor = node_editor(window)
    view = node_editor.view

    register_module(view.node_tree, math_nodes)
    register_module(view.node_tree, basic_nodes)
    register_module(view.node_tree, opencv_nodes)
    #item = NodeItem(view.scene())

    node_list = [
        'base.math',
        'input.value',
        'base.trigo',
        'base.compare',
        'input.image',
        'image.mix',
        'input.image'
    ]

    x = -1000
    for n in node_list :
        node = view.node_tree.nodes.add(n)
        node.location = (x,0)

        x += 200

    node.in0.prop.value = "C:/Users/Christophe/Desktop/IMG_20181028_163939__01_serieux.jpg"

    window.setCentralWidget(view)
    window.resize(1280, 720)
    #node.outputs[0].link_to(node2.inputs[0])

    window.setWindowTitle('Node Editor')
    window.show()
    app.exec_()
