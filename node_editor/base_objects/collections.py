from PySide2.QtWidgets import *
from PySide2.QtCore import *

from node_editor.base_objects.link import Link
from node_editor.base_objects.socket import *

from functools import partial

class Collection():
    def __init__(self):
        self._data = []

    def __setitem__(self, key, value):
        self._data.append((key, value))

    def __getitem__(self, key):
        if isinstance(key, int) : return self._data[key][1]
        elif isinstance(key, str) : return next(k[1] for k in self._data if k[0] == key)

    def __len__(self):
        return len(self._data)

    def to_list(self) :
        return [i[1] for i in self._data]

    def remove(self, i) :
        self._data.remove(next(k for k in self._data if k[1] == i))

class Links(list) :
    type = 'link'

    def add(self, from_socket, to_socket) :
        sockets = [from_socket, to_socket]

        #print(sockets[0].type)
        if sockets[0].type == 'input' : sockets = sockets[::-1]



        link = Link(*sockets)
        self.append(link)

        from_socket.link_changed.emit()
        to_socket.link_changed.emit()

        return link

    def remove(self, link) :
        link.link_item.remove()
        super().remove(link)

        link.from_socket.link_changed.emit()
        link.to_socket.link_changed.emit()

class Nodes(Collection) :
    type = 'node'

    def __init__(self, node_tree):
        super().__init__()
        self.view = node_tree.view
        self.node_tree = node_tree
        #self.scene = self.view.scene()

        ## Register Node Class
        self._nodes = {}

    def add(self, id_name) :
        node = self.node_tree.registered_nodes[id_name](self.node_tree)
        node.init()
        node.enabled = True
        node.execute()

        self[node.label] = node

        self.view.normalize_nodes_z()
        node.node_item.setZValue(node.node_item.zValue()+1)

        return node

    def remove(self, node) :
        # remove link first
        for l in node.links :
            l.remove()

        self.view.scene().removeItem(node.node_item)
        super().remove(node)

    def register(self, node) :
        self.node_tree.registered_nodes[node.id_name] = node





class Slots(Collection) :
    def __init__(self, node) :
        super().__init__()
        self.node = node

    def remove(self, name) :
        del self[name]


class InputSlots(Slots) :
    type = 'input'

    def __init__(self, node) :
        super().__init__(node)

    def add(self, socket_type, name = '') :
        socket_input = SocketInput(self, socket_type, name)

        self[name] = socket_input

        return socket_input


class OutputSlots(Slots) :
    type = 'output'

    def __init__(self, node) :
        super().__init__(node)

    def add(self, socket_type, name = '') :
        socket_output = SocketOutput(self, socket_type, name)

        self[name] = socket_output

        return socket_output

        '''
        self.init_socket(socket, parent =self, name =  name)
        socket.socket_item = SocketOutputItem(socket)
        #socket.prop.valueChanged.connect(self.node.display_result)


        widget = socket.create_result_widget()
        widget.setVisible(self.node.show_result)
        self.node.node_item.node_widget.result_layout.addWidget(widget)


        #result_label.setText.connect(socket.prop.valueChanged)

        self[name] = socket
        '''

        return socket


class PropertySlots(Slots) :
    type = 'property'

    def __init__(self, node) :
        super().__init__(node)

    def add(self, prop, name= '') :
        self.name = name
        #socket = PropertySocket(self, socket_type)

        prop.name = name
        #self.slot_layout = self.node.node_widget.properties_layout
        self.node.node_item.body.properties_layout.addWidget(prop)
        prop.value_changed.connect(self.node.execute)
        self[name] = prop

        return prop
