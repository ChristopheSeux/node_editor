from PySide2.QtCore import Qt

class MouseEvent :
    LMB = False
    RMB = False
    MMB = False

    def __init__(self, event = None, action = 'PRESS') :
        btn = {
            'LMB' : Qt.LeftButton,
            'RMB' : Qt.RightButton,
            'MMB' : Qt.MiddleButton
            }

        if not event : return

        for k,v in btn.items() :
            setattr(self, k, event.button() == v and action == 'PRESS')
