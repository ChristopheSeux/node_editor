from node_editor.graphics.link_item import LinkItem

class Link :
    def __init__(self, socket, to_socket) :
        self.from_node = socket.node
        self.to_node = to_socket.node

        self.from_socket = socket
        self.to_socket = to_socket


        self.link_item = LinkItem(self)

    def remove(self) :
        node_tree = self.from_socket.node.node_tree
        node_tree.links.remove(self)
