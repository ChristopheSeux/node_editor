from node_editor.base_objects.style import Style
from node_editor.graphics.node_item import NodeItem
from node_editor.base_objects.collections import InputSlots, OutputSlots, PropertySlots

import yaml

class Node :
    identifier = 'node_editor.Node'

    def __init__(self, node_tree) :
        self.node_tree = node_tree
        settings = self.node_tree.view.settings

        self.enabled = False

        # Default style for now
        self.style = settings.theme.node

        self.node_item = NodeItem(self)

        self.inputs = InputSlots(self)
        self.outputs = OutputSlots(self)
        self.properties = PropertySlots(self)

        self.show_result = False

    def to_dict(self) :
        return {
        'id_name' : self.id_name,
        'properties' : [p.value for p in self.properties],
        'inputs' : [s.value for s in self.inputs],
        'location' : [self.location.x(),self.location.y()]
        }

    def dump(self) :
        return yaml.dump(self.to_dict(),sort_keys=False)


    def execute(self) :
        pass

    def display_result(self) :
        pass


    def _on_input_changed(self) :
        if not self.enabled : return

        self.execute()
        self.node_item.node_widget.update()


    def remove(self) :
        self.node_tree.nodes.remove(self)


    @property
    def result(self):
        return self.node_item.result

    @property
    def location(self):
        return self.node_item.pos()

    # a setter function
    @location.setter
    def location(self, value):
        self.node_item.setPos(*value)

    @property
    def links(self) :
        return [l for l in self.node_tree.links if self in (l.from_socket.node,l.to_socket.node)]

    @property
    def select(self) :
        return self.node_item.isSelected()

    @select.setter
    def select(self, value) :
        self.node_item.setSelected(value)

        #self.invoke()
