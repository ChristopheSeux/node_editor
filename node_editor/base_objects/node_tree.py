from node_editor.base_objects.link import Link
from node_editor.base_objects.collections import Links
from node_editor.base_objects.collections import Nodes


class NodeTree :

    def __init__(self, view):

        self.view = view
        self.scene = view.scene()
        self.nodes = Nodes(self)
        self.links = Links()

        self.registered_nodes = {}
