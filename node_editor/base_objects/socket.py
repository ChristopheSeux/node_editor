from PySide2.QtWidgets import *
from PySide2.QtCore import *

from node_editor.graphics.socket_item import *
import time

class Socket(QObject) :
    link_changed = Signal()
    value_changed = Signal()
    #y_moved = Signal(QPointF)

    def __init__(self, parent, socket_type, name) :
        super().__init__()

        self.name = name
        self.parent = parent
        self.node = parent.node
        self.node_tree = self.node.node_tree
        self.socket_type = socket_type
        socket_type.parent = self
        self._value = None

        self.label = QLabel(name)

    @property
    def links(self) :
        return [l for l in self.node_tree.links if self in (l.from_socket, l.to_socket)]

    def remove_link(self, l) :
        self.node_tree.links.remove(l)

    def link_to(self, to_socket) :
        self.node_tree.links.add(self, to_socket)
        #to_socket

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        self.value_changed.emit()




class SocketInput(Socket) :
    def __init__(self, parent, socket_type, name = '') :
        super().__init__(parent, socket_type, name)

        self.type = 'input'
        self.prop = socket_type.prop
        self.prop.name = name
        self.value = self.prop.value


        self.prop.value_changed.connect(self.update)
        #self.prop.moveEvent = self._update_y
        #self.prop.resizeEvent = self._update_y



        self.value_changed.connect(self.node._on_input_changed)

        self.socket_item = SocketInputItem(self)

        #print(self.value)

    def update(self, value) :
        self.value = value



class SocketOutput(Socket) :
    def __init__(self, parent, socket_type, name = '') :
        super().__init__(parent, socket_type, name)

        self.type = 'output'
        self.value_changed.connect(self.update_links)

        self.socket_item = SocketOutputItem(self)

    def update_links(self) :
        for link in self.links :
            link.to_socket.value = self.value
