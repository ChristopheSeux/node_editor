from PySide2.QtWidgets import *
from PySide2.QtCore import *

from node_editor.widgets.base_widgets import *
from node_editor.widgets.properties import *
import numpy as np


class SocketType(QObject) :
    link_changed = Signal()
    value_changed = Signal()
    y_moved = Signal(QPointF)

    def __init__(self, name = '') :
        super().__init__()
        self.name = name
        self._value = None

    """
    def get_links(self) :
        return [l for l in self.node_tree.links if self in (l.from_socket, l.to_socket)]

    def remove_link(self, l) :
        self.node_tree.links.remove(l)

    def link_to(self, to_socket) :
        self.node_tree.links.add(self, to_socket)
        #to_socket
        """

    @property
    def value(self):
        return self.parent.value

    @value.setter
    def value(self, value):
        self.parent.value = value

    """
    def _update_y(self, event) :
        node_widget = self.node.node_item.node_widget
        co = self.prop.mapTo(node_widget,self.prop.rect().center())
        self.y_moved.emit(co)

    def _update_links(self, value) :
        for link in self.get_links() :
            link.to_socket.prop.value = value

    def _connect_prop(self, prop) :
        self.prop = prop
        self.prop.moveEvent = self.update_y
        self.prop.resizeEvent = self.update_y
        self.prop.value_changed.connect(self.value)

    def _create_result_widget(self) :
        self.result_widget = QLabel()
        self.value_changed.connect(self._update_links)
        self.value_changed.connect(self._update_result)

        return self.result_widget

    def _update_result(self, value) :
        self.result_widget.setText(str(value))


        self.value_changed.connect(self.execute)
        """

class SocketTypeInt(SocketType)  :
    def __init__(self, name='', value=0, min=None, max=None, unit='') :
        super().__init__(name)

        self.type = 'int'
        self.color = (140,140,140)
        self.prop = Number( name=name, value=value, min=min, max=max,
                            step=1, unit=unit)


class SocketTypeFloat(SocketType) :
    def __init__(self, name='', value=0, min=None, max=None, unit='', step = 0.01) :
        super().__init__(name)

        self.type = 'float'
        self.color = (160,160,160)
        self.prop = Number( name=name, value=value, min=min, max=max,
                            step=step, unit=unit)


class SocketTypeBool(SocketType) :
    def __init__(self, name = '') :
        super().__init__(name)

        self.type = 'bool'
        self.prop = Bool(name = name)
        self.color = (120,120,120)


class SocketTypeEnum(SocketType) :
    def __init__(self, name= '') :
        super().__init__(name)

        self.type = 'enum'
        self.prop = Enum(parent = node.node_item.node_widget, name = name)
        self.color = (80,80,240)


class SocketTypeString(SocketType) :
    def __init__(self, name ='') :
        super().__init__(name)

        self.type = 'string'
        self.prop = String(name = name)
        self.color = (80,80,240)


class SocketTypeColor(SocketType) :
    def __init__(self, name = '') :
        super().__init__(name)

        self.type = 'color'
        self.prop = Color(name = name)
        self.color = (255,255,0)


class SocketTypeImage(SocketType) :
    def __init__(self, name = '') :
        super().__init__(name)

        self.type = 'image'
        self.prop = Image(name = name)
        self.color = (255,255,0)
