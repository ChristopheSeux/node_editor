from attrdict import AttrDict
import os, json, yaml

class Style(AttrDict) :
    def __init__(self, style):
        if isinstance(style, dict) :
            super().__init__(style)

        elif os.path.exists(style) :
            with open(style) as data:
                if style.endswith(('yaml','yml')) :
                    style = yaml.safe_load(data)
                else :
                    style = json.loads(data)

        elif isinstance(style, str) :
            try :
                style = yaml.safe_load(style)
            except :
                style = json.loads(style)



        super().__init__(style)
