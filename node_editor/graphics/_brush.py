from PySide2.QtCore import QLineF, Qt
from PySide2.QtGui import QColor, QPen

class Brush() :
    def __init__(self, **kargs) :
        self.pen = Qt.NoPen

        if 'Lcolor' in kargs or kargs.get('Lwidth'):
            Lcolor = kargs.get('Lcolor', (10,10,10))
            Lwidth = kargs['Lwidth'] if 'Lwidth' in kargs else 1

            if not isinstance(Lcolor, QColor) : Lcolor = QColor(*Lcolor)

            self.pen = QPen(Lcolor)
            self.pen.setWidthF(Lwidth)

        if 'Fcolor' not in kargs :
            self.brush = Qt.NoBrush
        else :
            Fcolor = kargs['Fcolor']

            if not isinstance(Fcolor, QColor) : Fcolor = QColor(*Fcolor)

            self.brush = Fcolor

    def set(self, painter) :
        painter.setPen(self.pen)
        painter.setBrush(self.brush)
