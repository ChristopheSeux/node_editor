from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *

class LinkItem(QGraphicsPathItem):
    type = 'link'

    def __init__(self, link):
        super().__init__()

        self.link = link

        self.from_socket = self.link.from_socket.socket_item
        self.to_socket = self.link.to_socket.socket_item

        self.from_node = self.link.from_node.node_item
        self.to_node = self.link.to_node.node_item

        self.setEnabled(False)

        scn = link.from_node.node_tree.scene

        # Initialise path, create is boundbox
        self.draw_path()

        scn.addItem(self)

        self.setAcceptedMouseButtons(Qt.NoButton)

        self.setZValue(self.from_node.zValue())


    def remove(self) :
        scene = self.scene()
        if scene : scene.removeItem(self)

    def draw_path(self) :
        path = QPainterPath()
        start = self.from_socket.scenePos()
        end = self.to_socket.scenePos()

        path.moveTo(start)

        x1, y1 = start.x(), start.y()
        x2, y2 = end.x(), end.y()

        c1 = QPointF(x1 + abs((x2-x1)/2), y1)
        c2 = QPointF(x2 - abs((x1-x2)/2), y2)

        path.cubicTo(c1,c2, end)
        self.setPath(path)

    def paint(self, painter, option, widget) :
        set_draw_style(painter, width = 2, Lcolor =(180,180,180), cap_style = Qt.RoundCap)

        self.draw_path()

        painter.drawPath(self.path())
        #print(self.path())
