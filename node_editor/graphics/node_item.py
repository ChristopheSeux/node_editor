from PySide2.QtWidgets import *#(QWidget, QPushButton, QVBoxLayout, QHBoxLayout,
#QGraphicsItem, QLabel, QGraphicsDropShadowEffect,QFrame)
from PySide2.QtCore import * #QRect, QRectF, QLineF, Qt
from PySide2.QtGui import * #QPalette, QPainter, QColor, QPen, QPainterPath

from node_editor.widgets.base_widgets import *
#from node_editor.graphics.socket import *
from node_editor.utils import *



class NodeHeader(QWidget):
    def __init__(self, node_item):
        super().__init__()

        self.layout = HLayout(self)
        #self.layout.setSizeConstraint(QLayout.SetMinimumSize)
        self.label = QLabel(node_item.title)
        #self.header.setFixedHeight(25)
        set_font(self.label, 9, QFont.Bold)
        self.label.setStyleSheet("""
            background : transparent;
            color : rgb(240,240,240);
            padding : 3px 4px 3px 4px;
        """)

        self.layout.addWidget(self.label)


class NodeBody(QWidget):
    moved = Signal()

    def __init__(self, node_item):
        super().__init__()

        self.node_widget = node_item.node_widget

        self.layout = VLayout(self,spacing = 5, margins = (0,0,0,6))

        self.outputs_layout = VLayout(self.layout, spacing = 2, margins = (6,0,6,0), align = Qt.AlignRight)
        self.properties_layout = VLayout(self.layout, spacing = 2, margins = (6,0,6,0))
        self.inputs_layout = VLayout(self.layout, spacing = 4,margins = (6,0,6,0), align = Qt.AlignLeft)

    def get_y(self) :
        return self.mapToParent(self.rect().topLeft()).y()

    def moveEvent(self, event) :
        self.moved.emit()

    def resizeEvent(self, event) :
        self.moved.emit()



class NodeResult(QWidget):
    def __init__(self, node_item):
        super().__init__()

        self.node = node_item.node
        self.layout = VLayout(self)

    def set_widget(self, widget) :
        result_layout = self.layout
        for i in reversed(range(self.layout.count())):
            widget = result_layout.takeAt(i).widget()
            if widget is not None:
                widget.deleteLater()

        self.layout.addWidget(widget)


class NodeWidget(QGraphicsWidget):
    resized = Signal()

    def __init__(self, node_item):
        super().__init__()

        self.setParentItem(node_item)
        self.node_item = node_item
        self.layout = VGLayout(node_item.scene, parent = self)
        #self.layout.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        #self.layout.setPreferredHeight(0)

        self.resize(*node_item.size)



    def resizeEvent(self, event) :
        print('resize event')
        super().resizeEvent(event)
        self.resized.emit()



class NodeItem(QGraphicsObject):
    type = 'node'
    z_changed = Signal(float)

    def __init__(self, node, x=0, y=0):
        super().__init__()

        self.style = node.style
        self.scene = node.node_tree.scene
        self.title = node.label
        self.color = node.color
        self.node = node

        self.min_width = 112
        self.size = [self.min_width, 30]
        #self.node_widget = NodeWidget(self)
        self.node_widget = NodeWidget(self)
        #self.node_widget.setGraphicsEffect(shadow_effect(opacity = 60, offset=2))
        #self.proxy = self.scene.addItem(self.node_widget)
        #self.proxy.identifier = self.identifier

        #self.node_widget.setFlags(QGraphicsItem.ItemStacksBehindParent)


        #self.node_widget = QGraphicsWidget(self)
        #self.node_widget.setGraphicsEffect(shadow_effect())
        #self.node_widget.setOpacity(self.style.background.opacity/100)


        self.header = NodeHeader(self)
        self.body = NodeBody(self)
        self.result = NodeResult(self)

        self.node_widget.layout.addWidget(self.header, effect = shadow_effect())
        self.node_widget.layout.addWidget(self.result)
        self.node_widget.layout.addWidget(self.body, effect = shadow_effect(opacity = 60, offset=2))

        self.setAcceptHoverEvents(True)
        self.setFlags(
            QGraphicsItem.ItemIsSelectable |
            QGraphicsItem.ItemIsMovable |
            QGraphicsItem.ItemSendsGeometryChanges)

        self.scene.addItem(self)

        #self.set_style()

        #self.outline = ItemOutline(self, 4)
        #self.outline.setParentItem(self)
        #self.outline.hide()
        self.setGraphicsEffect(shadow_effect(radius = 8, offset = 2))
        #self.setOpacity(self.style.background.opacity/100)

        self.node_widget.resized.connect(self.resize)

        #self.node_widget.adjustSize()

    def adjustSize(self) :
        #self.header.adjustSize()
        #self.body.adjustSize()
        self.result.updateGeometry()
        self.result.adjustSize()
        #self.result.setWidth(self.size[0])

        self.node_widget.resize(112,self.node_widget.layout.preferredHeight())#resize(self.node_widget.layout.preferredSize())
        print(self.result.size(), self.size)


    def resize(self) :
        s = self.node_widget.size()
        self.size = [max(s.width(),self.min_width),s.height()]
        #self.node_widget.resize(*self.size)
        #self.outline.size = size
        #self.outline.update()

    def set_style(self) :

        self.setGraphicsEffect(shadow_effect())


    def itemChange(self, change, value):
        #print(QApplication.focusWidget())
        #if change == QGraphicsItem.ItemSelectedChange :
        #    self.outline.setVisible(value)


        return value



    def boundingRect(self):
        return QRect(0,0, *self.size)


    def paint(self, painter, options, widget):
        x,y = 0, 0
        w,h = self.size

        painter.setRenderHint(QPainter.Antialiasing)

        rect = QRectF(x, y, w, h)


        header_h = self.header.height()
        result_h = self.result.height()
        radius = 3
        header_color = QColor(*self.color)
        result_color = QColor(85,85,85)
        body_color = QColor(70,70,70)
        outline_color = QColor(230,230,230)

        header_pct = header_h / h
        result_pct = header_pct + result_h / h

        gradient= QLinearGradient(x, y, x, h)
        gradient.setColorAt(0, header_color.lighter(115))
        gradient.setColorAt(header_pct, header_color)


        if result_h and result_pct<1 :
            gradient.setColorAt(header_pct+0.001, result_color.darker(110))
            gradient.setColorAt(result_pct, result_color)
            gradient.setColorAt(result_pct+0.001, body_color)
        else :
            gradient.setColorAt(header_pct+0.001, body_color)


        painter.setBrush(gradient)

        if self.isSelected() :
            pen = QPen(outline_color)
        else :
            pen = QPen(body_color.darker(130))

        pen.setWidth(0)
        painter.setPen(pen)
        #set_draw_style(painter, Lcolor = (250,250,250),width = 0)
        painter.drawRoundedRect(rect, radius, radius,mode = Qt.AbsoluteSize)
        #painter.drawRect(0,0, *self.size)
