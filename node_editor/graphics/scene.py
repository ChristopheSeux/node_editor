from PySide2.QtWidgets import QGraphicsView, QGraphicsScene
from PySide2.QtCore import QRect, QLineF, Qt
from PySide2.QtGui import QPainter, QColor, QPen

from node_editor.utils import *


class Scene(QGraphicsScene) :
    def __init__(self, parent):
        super().__init__()

        self.style = parent.settings.theme.scene
        self.view = parent

        self.scene_size = [38400,21600]
        w, h = self.scene_size
        scene_rect = QRect(-w / 2, -h / 2, w, h)
        self.setSceneRect(scene_rect)



    def grid(self, left, right, top, bottom, size) :
        v_lines = [QLineF(x, top, x, bottom) for x in range(left,right,size)]
        h_lines = [QLineF(left, y, right, y) for y in range(top,bottom,size)]

        return v_lines+h_lines

    def drawBackground(self, painter, rect):
        grid_style = self.style.grid
        Lcolor = grid_style.line_color
        padding_color = grid_style.padding_color
        origin_color = grid_style.origin_color

        painter.setRenderHint(QPainter.Antialiasing, False)
        painter.setBrush(QColor(*self.style.background.color))
        painter.drawRect(rect)

        step = grid_style.size*grid_style.padding

        left = int(rect.left()) - (int(rect.left()) % step)
        top = int(rect.top()) - (int(rect.top()) % step)

        right = int(rect.right())
        bottom = int(rect.bottom())

        origin_lines = [QLineF(left, 0, right, 0),QLineF(0, top, 0, bottom)]

        Lwidth = 1/(self.view.zoom**0.5)
        if self.view.zoom >= 0.2 :
            set_draw_style(painter, Lcolor = Lcolor, width = Lwidth)
            painter.drawLines(self.grid(left, right, top, bottom, grid_style.size))

        set_draw_style(painter, Lcolor = padding_color, width = Lwidth*1.25)
        painter.drawLines(self.grid(left, right, top, bottom, step))

        set_draw_style(painter, Lcolor = origin_color, width = Lwidth*1.5)
        painter.drawLines(origin_lines)
