from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *

from node_editor.tools.socket_link import SocketLink
from node_editor.widgets.properties import *
from node_editor.widgets import properties

class SocketLabel(QLabel) :
    def __init__(self, socket):
        super().__init__()

        self.setFixedHeight(24)
        self.setText(socket.name)
        self.style = socket.node.style.slot

        self.location = QPointF()
        #print(self.style)

        #self.setAlignment(Qt.AlignVCenter)

        self.set_style()

    def set_style(self) :
        self.setStyleSheet(f"""
        QLabel {{
            color : rgb{self.style.text_color};
            font-size : 8pt;
            font-weight : 500;
            margin-left : 3px;
            margin-right : 3px;
            }}
        """)






class SocketWidget(QWidget):
    moved = Signal()

    def __init__(self, socket):
        super().__init__()

        self.node_item = socket.node.node_item

        self.layout = HLayout(self)
        self.label = SocketLabel(socket)
        self.layout.addWidget(self.label)

    def get_y(self) :
        body = self.node_item.body
        y = self.mapTo(body,self.rect().center()).y()

        return y + body.get_y()

    def moveEvent(self, event) :
        self.moved.emit()

    def resizeEvent(self, event) :
        self.moved.emit()



class SocketItem(QGraphicsItem):
    type = 'socket'

    def __init__(self, socket):
        super().__init__()

        self.socket = socket
        self.socket_type = socket.socket_type
        self.node_item = socket.node.node_item
        self.style = socket.node.style.slot
        self.hover = False

        self.size = (self.style.width, self.style.height)

        self.setAcceptDrops(True)
        self.setAcceptHoverEvents(True)
        self.setFlags(  QGraphicsItem.ItemIsSelectable |
                        QGraphicsItem.ItemSendsGeometryChanges )


        self.socket_widget = SocketWidget(socket)
        self.socket_widget.moved.connect(self.on_moved)
        self.node_item.body.moved.connect(self.on_moved)
        self.node_item.node_widget.resized.connect(self.on_moved)


    def mousePressEvent(self, event):
        SocketLink(self)

    def hoverEnterEvent(self, event):
        self.hover = True
        self.update()

    def hoverLeaveEvent(self, event):
        self.hover = False
        self.update()

    def boundingRect(self):
        return QRectF(-self.size[0]*0.5,-self.size[1]*0.5, *self.size)

    def paint(self, painter, options, widget):
        set_draw_style(painter, Fcolor = self.socket_type.color, Lcolor = (40,40,40), width = 0)


        hover_size = 1 if self.hover else 0.7
        r = self.size[0]*0.5*hover_size, self.size[1]*0.5*hover_size
        painter.drawEllipse(QPointF(0,0), *r)


class SocketInputItem(SocketItem):
    def __init__(self, socket):
        super().__init__(socket)

        self.prop = socket.prop

        self.socket_widget.label.setAlignment(Qt.AlignVCenter|Qt.AlignLeft)
        self.socket_widget.label.hide()


        ## Add socket Widget to input layout
        self.slot_layout = self.node_item.body.inputs_layout
        self.socket_widget.layout.addWidget(self.prop)
        self.slot_layout.addWidget(self.socket_widget)

        self.setParentItem(self.node_item)

        self.socket.link_changed.connect(self.hide_prop)

    @Slot()
    def on_moved(self) :
        self.setPos(0, self.socket_widget.get_y())

    def hide_prop(self) :
        is_linked = len(self.socket.links)

        if is_linked :
            l = self.socket.links[0]
            self.socket.update(l.from_socket.value)

        else :
            self.socket.update(self.prop.value)

        self.socket_widget.label.setVisible(is_linked)
        self.prop.setVisible( not is_linked)


class SocketOutputItem(SocketItem):
    def __init__(self, socket):
        super().__init__(socket)

        self.socket_widget.label.setAlignment(Qt.AlignVCenter|Qt.AlignRight)

        ## Add socket Widget to output layout
        self.slot_layout = self.node_item.body.outputs_layout
        self.slot_layout.addWidget(self.socket_widget)

        #self.label.y_moved.connect(self.set_y)

        #self.setX(self.node_item.size[0])
        self.setParentItem(self.node_item)

        self.result_layout = self.node_item.result.layout
        #self.result_layout.addWidget(socket.result)

        #if not socket.node.show_result :
        #    socket.result.hide()


    @Slot()
    def on_moved(self) :
        self.setPos(self.node_item.size[0], self.socket_widget.get_y())

'''
class SocketPropertyItem(SocketItem):
    def __init__(self, socket):
        super().__init__(socket)

        self.slot_layout = self.body.properties_layout
        self.slot_layout.addWidget(self.socket_type.prop)
'''
