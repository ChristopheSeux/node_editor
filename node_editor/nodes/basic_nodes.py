from node_editor.base_objects.node import Node
from node_editor.base_objects.socket_type import *
from node_editor.widgets.properties import *


class NodeValue(Node) :
    label = 'Value'
    id_name = 'input.value'
    color = (100,100,100)

    def init(self):
        self.in0 = Number()
        self.properties.add(self.in0, 'Value')

        self.out = SocketTypeFloat()
        self.outputs.add(self.out, 'Value')


    def execute(self) :
        self.out.value = self.in0.value
