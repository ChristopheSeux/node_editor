
from node_editor.base_objects.node import Node
from node_editor.base_objects.socket_type import *
from node_editor.widgets.properties import *

from math import *

class NodeMath(Node) :
    label = 'Math'
    id_name = 'base.math'
    color = (75,135,175)

    def init(self):
        self.show_result = True

        self.operation = Enum()
        self.operation.addItems((
            'Add', 'Substract', 'Multiply', 'Divide',
            'Power','Logarithm',
            'Round','Modulo','Absolute',
            ))

        self.properties.add(self.operation, 'Operation')

        self.clamp = Bool()
        self.properties.add(self.clamp, 'Clamp')

        self.in_value1 = SocketTypeInt()
        self.inputs.add(self.in_value1, 'Value')

        self.in_value2 = SocketTypeInt()
        self.inputs.add(self.in_value2, 'Value')

        self.out_value = SocketTypeInt(min = -100, max = 100)
        self.outputs.add(self.out_value, 'Value')

        self.preview = ResultText()
        self.result.set_widget(self.preview)

    def execute(self) :
        in1 = self.in_value1.value
        in2 = self.in_value2.value

        if self.operation.value == 'Add' :
            out = in1 + in2
        if self.operation.value == 'Substract' :
            out = in1 - in2
        if self.operation.value == 'Multiply' :
            out = in1 * in2
        if self.operation.value == 'Divide' :
            out = in1 / in2
        if self.operation.value == 'Power' :
            out = in1**in2
        if self.operation.value == 'Logarithm' :
            out = log(in1, in2)
        if self.operation.value == 'Round' :
            out = round(in1 , in2)
        if self.operation.value == 'Modulo' :
            out = in1 % in2
        if self.operation.value == 'Absolute' :
            out = abs(in1)


        if self.clamp.value :
            out = np.clip(out,0 ,1)

        #print(in1,in2, self.out_value.value)
        self.out_value.value = out

        # Display Result
        if isinstance(out, float) : out = round(out,3)
        self.preview.setText(str(out))


class NodeCompare(Node) :
    label = 'Compare'
    id_name = 'base.compare'
    color = (75,135,175)

    def init(self):
        self.show_result = True

        self.operation = Enum()
        self.operation.addItems(('Less Than','Greather Than','Egal','Minimum','Maximum'))
        self.properties.add(self.operation, 'Operation')

        self.in_value1 = SocketTypeInt()
        self.inputs.add(self.in_value1, 'Value')

        self.in_value2 = SocketTypeInt()
        self.inputs.add(self.in_value2, 'Value')

        self.out_value = SocketTypeBool()
        self.outputs.add(self.out_value, 'Value')

        self.preview = ResultText()
        self.result.set_widget(self.preview)

    def execute(self) :
        in1 = self.in_value1.value
        in2 = self.in_value2.value

        if self.operation.value == 'Less Than' :
            out = in1 < in2
        if self.operation.value == 'Greather Than' :
            out = in1 > in2
        if self.operation.value == 'Egal' :
            out = in1 == in2
        if self.operation.value == 'Minimum' :
            out = min(in1, in2)
        if self.operation.value == 'Maximum' :
            out = max(in1, in2)

        self.out_value.value = out

        # Display Result
        if isinstance(out, float) : out = round(out,3)
        self.preview.setText(str(out))


class NodeTrigo(Node) :
    label = 'Trigo'
    id_name = 'base.trigo'
    color = (75,135,175)

    def init(self):
        self.show_result = True

        self.operation = Enum()
        self.operation.addItems(('Sin', 'Cos', 'Tangent', 'Arcsin', 'Arcos','Arctangent'))
        self.properties.add(self.operation, 'Operation')

        self.in_value1 = SocketTypeInt()
        self.inputs.add(self.in_value1, 'Value')

        self.clamp = Bool()
        self.properties.add(self.clamp, 'Clamp')

        self.out_value = SocketTypeInt()
        self.outputs.add(self.out_value, 'Value')

        self.preview = ResultText()
        self.result.set_widget(self.preview)

    def execute(self) :
        in1 = self.in_value1.value

        if self.operation.value == 'Sin' :
            out = sin(in1)
        if self.operation.value == 'Cos' :
            out = cos(in1)
        if self.operation.value == 'Tangent' :
            out = tan(in1)
        if self.operation.value == 'Arcsin' :
            out = asin(in1)
        if self.operation.value == 'Arcos' :
            out = acos(in1)
        if self.operation.value == 'Arctangent' :
            out = atan(in1)

        if self.clamp.value :
            out = np.clip(out,0 ,1)

        self.out_value.value = out

        # Display Result
        if isinstance(out, float) : out = round(out,3)
        self.preview.setText(str(out))
