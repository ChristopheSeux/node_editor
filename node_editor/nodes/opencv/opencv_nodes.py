from node_editor.base_objects.node import Node
from node_editor.base_objects.socket_type import *
from node_editor.widgets.properties import *

from PySide2.QtGui import QImage, QPixmap

import numpy as np
import cv2

from . import blend_modes

from time import time


def get_preview(img, size = 128) :
    #img = img.get()

    h, w, c = img.shape
    dw, dh = size, int((h*size)/w) ## Display with ratio
    preview = cv2.resize(img, dsize=(dw, dh), interpolation=cv2.INTER_CUBIC)

    fmt = QImage.Format_RGBA8888 if c == 4 else QImage.Format_RGB888

    preview = preview.astype(np.uint8)
    #print(preview)
    #preview = preview.astype(int)
    #print(preview)
    #print('fmt',fmt)
    #fmt = QImage.Format_RGB888
    #print(dw, dh)

    return  QPixmap(QImage(preview, dw, dh, c*dw, fmt))

def set_alpha_channel(img) :
    h, w, c = img.shape

    if c == 3 :
        img = np.dstack(( img, numpy.zeros((25, 54)) ) )

    return img

class NodeImage(Node) :
    label = 'Image'
    id_name = 'input.image'
    color = (170,130,40)

    def init(self):
        self.show_result = True

        self.in0 = SocketTypeString()
        self.inputs.add(self.in0, 'Path')

        self.in0.prop.value = r"C:\Users\Christophe\Desktop\CSeux_permis.png"

        self.out = SocketTypeImage()
        self.outputs.add(self.out, 'Rgba')

        self.preview = ResultLabel()

        self.result.set_widget(self.preview)
        #self.preview.setScaledContents( True )

        #self.preview.setSizePolicy( QSizePolicy.Fixed, QSizePolicy.Minimum )

    def execute(self) :
        #pix = QPixmap(self.in0.value)
        #img = cv2.imread(self.in0.value,-1)[...,::-1]
        img = cv2.imread(self.in0.value,cv2.IMREAD_COLOR)[...,::-1]
        #img = cv2.cvtColor(img, cv2.COLOR_RGB2RGBA).astype(float)
        #print(img)
        self.out.value = img

        self.preview.setPixmap(get_preview(img))

        self.node_item.adjustSize()




class NodeGaussianBlur(Node) :
    label = 'Blur'
    id_name = 'image.gaussian_blur'
    color = (170,130,40)

    def init(self):
        self.show_result = True

        self.border_type = Enum()
        self.border_type.addItems((
            'Default', 'Constant', 'Replicate',
            'Reflect ','Wrap', 'Isolated'))

        self.properties.add(self.border_type, 'Border Type')

        self.src  = SocketTypeColor()
        self.inputs.add(self.src, 'Src')

        self.ksize = SocketTypeFloat()
        self.inputs.add(self.ksize, 'Ksize')

        self.x = SocketTypeFloat()
        self.inputs.add(self.x, 'Sigma X')

        self.y = SocketTypeFloat()
        self.inputs.add(self.y, 'Sigma Y')

        self.dst = SocketTypeColor()
        self.outputs.add(self.dst, 'Dst')

        self.preview = ResultLabel()
        self.result.set_widget(self.preview)

    def execute(self) :
        src = self.src.value
        ksize = self.ksize.value
        x  = int(self.x.value* ksize)
        y = int(self.y.value * ksize)

        if x<1 : x = 1
        if y<1 : y = 1


        border_type = 'BORDER_'+ self.border_type.value.upper()

        print(f"""
        src : {src}
        ksize : {ksize}
        x, y : {x, y}
        border_type : {getattr(cv2, border_type)}
        """)


        img = cv2.blur(src, (x, y), borderType =getattr(cv2, border_type))

        self.dst.value = img
        self.preview.setPixmap(get_preview(img))

        self.node_item.adjustSize()


class NodeMix(Node) :
    label = 'Mix'
    id_name = 'image.mix'
    color = (170,130,40)

    def init(self):
        self.show_result = True

        self.operation = Enum()
        self.operation.addItems((
            'Addition', 'Darken Only', 'Difference', 'Divide',
            'Dodge','Hard Light', 'Lighten Only','Multiply',
            'Overlay','Screen', 'Soft Light', 'Subtract' ))

        self.properties.add(self.operation, 'Operation')


        self.mask = SocketTypeFloat(min = 0, max = 1)
        self.inputs.add(self.mask, 'Mask')

        self.background = SocketTypeColor()
        self.inputs.add(self.background, 'Background')

        self.foreground = SocketTypeColor()
        self.inputs.add(self.foreground, 'Foreground')



        self.out = SocketTypeColor()
        self.outputs.add(self.out, 'Rgb')

        self.preview = ResultLabel()
        self.result.set_widget(self.preview)

    def execute(self) :
        try :

            blend_func = getattr(blend_modes, self.operation.value.replace(' ','_').lower())

            in1 = self.background.value
            in2 = self.foreground.value

            in1 = cv2.cvtColor(in1, cv2.COLOR_RGB2RGBA).astype(float)
            in2 = cv2.cvtColor(in2, cv2.COLOR_RGB2RGBA).astype(float)

            t0 = time()

            img = blend_func(in1, in2, self.mask.value)

            #◙img = cv2.multiply(in1, in2, 2)
            #h, w, c = img.get().shape
            #divide_img = np.zeros([h,w,c],dtype=np.uint8)
            #divide_img[:] = 2


            #img = cv2.divide(img, divide_img)

            print(time()-t0)

            #print(img)
            self.out.value = img.astype(int)
            self.preview.setPixmap(get_preview(img))
        except Exception as e:
            print(e)

            self.preview.setText('No Image')

        self.node_item.adjustSize()
