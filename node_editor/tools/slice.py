from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *

class Slice(QGraphicsLineItem):
    """
    Base Pipe item used for drawing node connections.
    """

    def __init__(self, scene):
        super().__init__()

        #self.link = link

        self.mouse = QPointF(10,10)
        scene.addItem(self)
        self.setFlags(  QGraphicsItem.ItemIsMovable |
                        QGraphicsItem.ItemIsSelectable |
                        QGraphicsItem.ItemSendsGeometryChanges)
        print('init Socket')

        self.grabMouse()

    def mousePressEvent(self, event):
        event.accept()
        self.mouse = event.scenePos()
        self.update()
        print("Socket link press")

    def mouseMoveEvent(self, event):
        event.accept()
        self.mouse = event.scenePos()
        self.update()
        print("Socket link move")

    def mouseReleaseEvent(self, event):
        event.accept()
        self.ungrabMouse()
        print("Socket link release")

    #def boundingRect(self) :
    #    return QRectF()



    def paint(self, painter, option, widget) :
        set_draw_style(painter, width = 2.5, Lcolor =(255,180,180), cap_style = Qt.RoundCap)

        self.path = QLineF(QPointF(),self.mouse)
        self.setLine(self.path)

        painter.drawLine(self.line())
        #print(self.path())
