from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *

#from node_editor.widgets.socket import SocketItem

class SocketLink(QGraphicsPathItem):
    """
    Base Pipe item used for drawing node connections.
    """

    def __init__(self, socket):
        super().__init__()

        self.socket_item = socket
        self.mouse = socket.scenePos()

        links = self.socket_item.socket.links

        #print(links)

        if self.socket_item.socket.type == 'input' and links :
            l = links[0]
            self.socket_item = l.from_socket.socket_item
            self.socket_item.socket.remove_link(l)

        self.mouse_start = self.socket_item.scenePos()
        #self.setPos(self.mouse_start)

        self.node_item = self.socket_item.node_item

        self.draw_path()

        socket.scene().addItem(self)

        self.grabMouse()
        self.socket_hover = None



    '''
    def boundingRect(self):
        return self.scene().sceneRect()
    '''

    def is_item_valid(self, item) :
        return ( item.node_item != self.node_item
                 and item.socket.type != self.socket_item.socket.type)

    def mouseMoveEvent(self, event):
        self.mouse = event.scenePos()
        self.update()

        items = [i for i in self.scene().items(event.scenePos(), Qt.IntersectsItemShape)
                if i.type == 'socket']

        item_hover = items[0] if items else None
        if item_hover and self.is_item_valid(item_hover):
            self.socket_hover = item_hover
            item_hover.hover = True
            item_hover.update()
            self.mouse = item_hover.scenePos()

        elif self.socket_hover :
            self.socket_hover.hover = False
            self.socket_hover.update()
            self.socket_hover = None

            #self.ungrabMouse()
        #print(self.scene().itemAt(event.scenePos(), QTransform()))

    def mouseReleaseEvent(self, event):

        if self.socket_hover :
            for l in self.socket_hover.socket.links :
                print('remove, ',l)
                self.socket_hover.socket.remove_link(l)

            self.socket_item.socket.link_to(self.socket_hover.socket)

        #print('ungrab')
        self.ungrabMouse()
        self.scene().removeItem(self)
        #self.ungrabMouse()


    def draw_path(self) :
        path = QPainterPath()
        path.moveTo(self.mouse_start)

        invert = -1 if self.socket_item.socket.type == 'input' else 1

        x1, y1 = self.mouse_start.x(), self.mouse_start.y()
        x2, y2 = self.mouse.x(), self.mouse.y()

        c1 = QPointF(x1 + abs((x2-x1)/2)*invert, y1)
        c2 = QPointF(x2 - abs((x1-x2)/2)*invert, y2)

        path.cubicTo(c1,c2,self.mouse)
        self.setPath(path)

    def paint(self, painter, option, widget) :
        set_draw_style(painter, width = 2.5, Lcolor =(160,160,160), cap_style = Qt.RoundCap)

        self.draw_path()

        painter.drawPath(self.path())
        #print(self.path())
