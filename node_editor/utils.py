from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

import yaml
from attrdict import AttrDict
import os
from os.path import *
import inspect

MODULE_PATH = dirname(__file__)
os.chdir(MODULE_PATH)


def register_module(node_tree, module) :
    for m in [m for m in inspect.getmembers(module, inspect.isclass)] :
        if hasattr(m[1],'identifier') and m[1].identifier == 'node_editor.Node' and m[0]!= 'Node':
            node_tree.nodes.register(m[1])
        #if any([c== Node for c in inspect.getmro(m[1])]) :
        #    print(m[0])


def read_settings() :
    with open(abspath("settings.yml")) as data:
        return AttrDict(yaml.safe_load(data))

def icon_path(icon_name) :
    return join(MODULE_PATH, 'resources', 'icons', icon_name+'.png').replace('\\','/')

def set_font(widget, size, weight) :
    font = widget.font()
    font.setPointSize(size)
    font.setWeight(weight)
    widget.setFont(font)

def shadow_effect(color = None, opacity = 100, radius = 6, offset=3) :
    effect = QGraphicsDropShadowEffect()
    effect.setBlurRadius(radius)
    effect.setOffset(offset,offset)
    if not color : color = (0,0,0,opacity)
    effect.setColor(QColor(*color))

    return effect

def set_draw_style(painter, **kargs) :
    if 'Lcolor' not in kargs and not kargs.get('width'):
        painter.setPen(Qt.NoPen)

    else :
        Lcolor = kargs.get('Lcolor',(10,10,10))
        width = kargs['width'] if 'width' in kargs else 1

        if not isinstance(Lcolor, QColor) :
            Lcolor = QColor(*Lcolor)

        pen = QPen(Lcolor)
        pen.setWidthF(width)
        pen.setCapStyle(kargs.get('cap_style',Qt.SquareCap))

        painter.setPen(pen)

    if 'Fcolor' not in kargs :
        brush = Qt.NoBrush
    else :
        Fcolor = kargs['Fcolor']
        if not isinstance(Fcolor, QColor) :
            brush = QColor(*Fcolor)

    if hasattr(painter, 'setBrush') :
        painter.setBrush(brush)

def set_css(widget, css) :
    if not isinstance(css, dict) : css = yaml.load(css, Loader = yaml.SafeLoader)

    style = ''
    for w, data in css.items() :
        style += '%s {\n'%w
        for k, value in data.items() :
            style +='    %s :'%k
            if isinstance(value, (str, int, float)) :
                style += ' %s'%value
            else :
                if not isinstance(value, (list, tuple)) : value = [value]
                for v in value :
                    if v['unit'] in ('px',) :
                        style += ' %s%s'%(v['value'],v['unit'])

                    elif v['unit'] in ('rgb','rgba') :
                        style += ' %s%s'%(v['unit'] ,str(tuple(v['value'])))
            style += ';\n'


        style+='}\n'

    widget.setStyleSheet(style)
