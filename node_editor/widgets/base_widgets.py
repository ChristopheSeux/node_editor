from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *



class Layout :
    settings = {'spacing' : 0, 'parent' : None, 'margins' : 0, 'align' : None}

    def set_settings(self, **kargs) :
        self.settings.update(kargs)
        if 'spacing' in kargs :
            self.setSpacing(kargs['spacing'])

        if 'margins' in kargs :
            m = kargs['margins']
            if isinstance(m, (float, int)) : m = (m,)*4
            self.setContentsMargins(*m)

        if kargs.get('parent') :
            if isinstance(kargs['parent'], (QBoxLayout,QGraphicsLinearLayout)) :
                kargs['parent'].addLayout(self)
            else :
                kargs['parent'].setLayout(self)

        if kargs.get('align') :
            if isinstance(kargs['parent'], QGraphicsLinearLayout) :
                self.setAlignment(self, kargs['align'])
            else :
                self.setAlignment(kargs['align'])

class GLayout(Layout, QGraphicsLinearLayout) :
    def __init__(self, scene):
        super().__init__()
        self.scene = scene

    def addWidget(self, widget, effect = None) :
        widget.setAttribute(Qt.WA_TranslucentBackground)
        proxy = self.scene.addWidget(widget)

        if effect :
            proxy.setGraphicsEffect(effect)

        self.addItem(proxy)

    def addLayout(self, layout) :
        print(layout)
        self.addItem(layout)

class HLayout(QHBoxLayout, Layout) :
    def __init__(self, parent= None, spacing = 0, margins = 0, align = None):
        super().__init__()

        self.set_settings(parent = parent, spacing = spacing, margins = margins, align = align)

class VLayout(QVBoxLayout, Layout) :
    def __init__(self, parent = None, spacing = 0, margins = 0, align = None):
        super().__init__()

        self.set_settings(parent = parent, spacing = spacing, margins = margins, align = align)


class HGLayout(GLayout) :
    def __init__(self, scene, parent = None, spacing = 0, margins = 0, align = None):
        super().__init__(scene)

        self.setOrientation(Qt.Horizontal)
        self.set_settings(parent = parent, spacing = spacing, margins = margins, align = align)

class VGLayout(GLayout) :
    def __init__(self, scene, parent = None, spacing = 0, margins = 0, align = None):
        super().__init__(scene)

        self.setOrientation(Qt.Vertical)
        self.set_settings(parent = parent, spacing = spacing, margins = margins, align = align)


class ResultText(QLabel) :
    def __init__(self, parent = None, text = ''):
        super().__init__(text, parent=parent)

        self.setAlignment(Qt.AlignCenter)

        self.setStyleSheet("""
        QLabel {
            background : transparent;
            color : rgb(190,190,190);
            padding : 5;
            font-weight: 600;
            font-size: 8pt;
            }
        """)

class ResultLabel(QLabel) :
    def __init__(self, parent = None, text = ''):
        super().__init__(text, parent=parent)

        self.setAlignment(Qt.AlignCenter)

        self.setStyleSheet("""
        QLabel {
            background : transparent;
            color : rgb(190,190,190);
            padding : 3;
            font-weight: 600;
            font-size: 8pt;
            }
        """)







class LineEdit(QLineEdit) :
    def __init__(self, parent = None):
        super().__init__(parent)
        self.set_style()
        self.canceled = False
        self.setFixedHeight(25)

    def focusOutEvent(self, event) :
        self.canceled = True
        self.editingFinished.emit()
        super().focusOutEvent(event)


    def mousePressEvent(self, event) :
        if event.button() in (Qt.RightButton,) :
            self.canceled = True
            self.editingFinished.emit()
        else :
            super().mousePressEvent(event)

    def keyPressEvent(self, event):
        if event.key() in (Qt.Key_Escape,) :
            self.canceled = True
            self.editingFinished.emit()
        else :
            super().keyPressEvent(event)

    def set_style(self) :
        self.setTextMargins(4, -5, 4, -5)
        set_css(self,"""
        QLineEdit :
            color :  rgb(220,220,220)
            background-color : rgb(80,80,80)
            selection-background-color : rgb(10,10,10)
            height : 50px
            border-style : none
            border-radius : 4px
            font-weight: 600
            font-size: 8pt
        """)



class HSlider(QDoubleSpinBox) :
    clicked = Signal()

    def __init__(self,  parent = None, name = '',value = 0, min =None, max =None, step = 0.01, unit = ''):
        super().__init__(parent)
        self.unit = unit
        self._x = 0
        self._value = 0

        self.use_offset = False
        self.step = step
        self.name = name
        self.hover = False

        self.min = min
        self.max = max

        if min is None : min = -float('inf')
        if max is None : max = float('inf')

        self.setMinimum(min)
        self.setMaximum(max)
        self.setValue(value)



        #self.value = value
        self.lineEdit().hide()

        #self.setButtonSymbols(QAbstractSpinBox.NoButtons)


    def value_from_mouse(self,event) :
        return QStyle.sliderValueFromPosition(
                    self.minimum(), self.maximum(),
                    event.x(), self.width())


    def enterEvent(self, event) :
        self.hover = True

    def leaveEvent(self, event) :
        self.hover = False

    def mousePressEvent(self, event) :
        self._x = event.x()
        self._value = self.value()


        opt = QStyleOptionSpinBox()
        self.initStyleOption(opt)

        if (self.style().subControlRect(QStyle.CC_SpinBox, opt, QStyle.SC_SpinBoxUp).contains(event.pos())) :
            print('up')
        elif (self.style().subControlRect(QStyle.CC_SpinBox, opt, QStyle.SC_SpinBoxDown).contains(event.pos())) :
            print('down')


    def mouseMoveEvent(self, event):
        if self.use_offset :
            offset = (event.x()-self._x)/ self.width()

            if self.min is not None and self.max is not None :
                pos = offset * (self.maximum() - self.minimum())
            else :
                pos = offset *10
            #self.value = self._value + pos
            self.setValue(self._value + pos)

        else :
            #self.value = self.value_from_mouse(event)
            self.setValue(self.value_from_mouse(event))

            print('Value from mouse')
        self.valueChanged.emit(self.value())


    def mouseReleaseEvent(self, event):
        if self._x == event.x() :
            self.clicked.emit()



    def paintEvent(self, event):

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(Qt.NoPen)

        w, h = self.width()-1, self.height()-1
        x, y = 0.5, 0.5
        #value = self.
        #☺minimum(), max =

        rect = QRect(x, y, w, h)


        bg_color = QColor(90,90,90) if self.hover else QColor(80,80,80)

        radius = 3

        #Get Value
        pos = self.value() / (self.maximum() - self.minimum())
        #value_rect = QRect(0,0,pos*rect.width(),rect.height())

        slider_color =  (85,127,193)
        # Draw background


        gradient= QLinearGradient(x, y, w, y)
        gradient.setColorAt(0, QColor(*slider_color))
        gradient.setColorAt(pos, QColor(*slider_color))
        if pos <1 :
            gradient.setColorAt(pos+0.0001, bg_color)

        overlay = QLinearGradient(x, y, x, h)
        overlay.setColorAt(0, QColor(255,255,255,20))
        overlay.setColorAt(1, QColor(0,0,0,0))


        contour = QLinearGradient(x, y, x, h)
        contour.setColorAt(0, QColor(60,60,60))
        contour.setColorAt(1, QColor(50,50,50))

        #painter.fillRect(rect, QBrush(QColor(50, 50, 50)))
        #pen = QPen(gradient, 4.0)
        painter.setBrush(gradient)
        painter.drawRoundedRect(rect, radius, radius,mode = Qt.AbsoluteSize)
        painter.setBrush(overlay)
        painter.drawRoundedRect(rect, radius, radius,mode = Qt.AbsoluteSize)

        painter.setBrush(Qt.NoBrush)
        painter.setPen(QPen(contour,1))
        painter.drawRoundedRect(rect, radius, radius,mode = Qt.AbsoluteSize)
        #painter.setBrush(Qt.NoBrush)
        #painter.drawRoundedRect(rect, radius, radius,mode = Qt.AbsoluteSize)



        set_font(painter, 8, QFont.DemiBold)

        #Draw Value
        painter.setPen(QColor(250, 250, 250,200))

        txt_rect = QRect(6,0,self.width()-8, self.height())

        value_txt = f'{self.value()} {self.unit}'
        if self.name :
            painter.drawText(txt_rect,Qt.AlignLeft|Qt.AlignVCenter, self.name)
            painter.drawText(txt_rect,Qt.AlignRight|Qt.AlignVCenter, value_txt)
        else :
            painter.drawText(txt_rect,Qt.AlignCenter|Qt.AlignVCenter, value_txt)
