from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *
from node_editor.widgets.base_widgets import *
from node_editor.widgets.properties import *

class NodeSearch(QDialog) :
    def __init__(self, view):
        super().__init__(view)
        self.setAutoFillBackground(True)
        self.layout = VLayout(self)
        self.view = view

        self.header = QFrame(self)
        self.header.setFixedHeight(30)
        self.header.layout = HLayout(self.header, margins = (8,0,8,0))
        self.label = QLabel('Search')
        self.header.layout.addWidget(self.label)

        self.layout.addWidget(self.header)


        ## SEARCH
        self.body = QFrame()
        self.body.layout = VLayout(self.body,margins = (8,4,8,4), spacing = 4)
        self.layout.addWidget(self.body)

        self.search = LineEdit(self)
        self.search.textChanged.connect(self.filter_items)
        self.search.focusOutEvent = self.focusOutEvent
        self.body.layout.addWidget(self.search)

        self.list_widget = QListWidget(self)
        set_font(self.list_widget, 9, QFont.DemiBold)
        self.list_widget.focusOutEvent = self.focusOutEvent
        self.list_widget.itemPressed.connect(self.add_node)

        for id_name, node in self.view.node_tree.registered_nodes.items() :
            i = QListWidgetItem(node.label)
            i.id_name = id_name
            self.list_widget.addItem(i)

        self.body.layout.addWidget(self.list_widget)

        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setWindowFlags(Qt.Dialog | Qt.FramelessWindowHint)

        self.resize(132, 200)

        set_css(self,"""
        QListWidget :
            border-style : none
        QListWidget::item :
            height : 22px
            color : rgb(200,200,200)
        QListWidget::item:hover :
            border-style : none
            height : 22px
            background-color : rgb(100,100,100)
        QListWidget::item:selected  :
            border-style : none
            background-color : rgb(120,120,120)
        QLabel :
            color : rgb(220,220,220)
            font-weight: 600
            font-size: 9pt
        QFrame :
            background-color : rgb(80,80,80)
        """)

        set_css(self.header,"""
        QFrame :
            border-style: node
            background-color : rgb(100,100,100)
        """)

        set_css(self.body,"""
        QFrame :
            border-style: node
            background-color : rgb(55,55,55)
        """)

    def add_node(self, item) :
        self.view.scene().clearSelection()
        node = self.view.node_tree.nodes.add(item.id_name)
        node.select = True
        node.location = self.view.get_mouse_pos()
        self.close()

    def filter_items(self) :
        for i in range(self.list_widget.count()) :
            item = self.list_widget.item(i)
            item.setHidden(self.search.text().lower() not in item.text().lower())
            #print(self.list_widget.item(i).text())

    def showEvent(self, event) :
        pos = QCursor().pos()
        self.move(pos.x()- self.width()/2, pos.y()-self.header.height()/2) # center on header



    def focusOutEvent(self, event) :
        if QApplication.focusWidget() not in (self, self.search,self.list_widget) :
            self.close()
