from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from node_editor.utils import *
from node_editor.widgets.base_widgets import *

import numpy as np

class Number(QWidget) :
    value_changed = Signal(float)
    def __init__(self, parent = None, name='', value=0, min=None, max=None, step = 0.01,unit='') :
        super().__init__(parent)


        self.setFixedHeight(24)
        #self.name = name
        self.layout = HLayout(self)
        self.slider = HSlider(self, name, value, min, max, step, unit)
        self.slider.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.slider.setFocusPolicy(Qt.NoFocus)
        #self.slider.setFixedHeight()
        self.slider.use_offset = True
        self.slider.clicked.connect(self.edit_value)

        self.line_edit = LineEdit()
        self.line_edit.setSizePolicy(self.sizePolicy())
        self.line_edit.editingFinished.connect(self.editing_finished)

        self.layout.addWidget(self.slider)
        self.layout.addWidget(self.line_edit)

        self.line_edit.setVisible(False)

        self.slider.valueChanged.connect(self.on_value_changed)

    def on_value_changed(self, value) :
        self.value_changed.emit(value)

    def edit_value(self) :
        self.line_edit.canceled = False
        self.line_edit.setText(str(self.slider.value()))
        self.line_edit.setVisible(True)
        self.line_edit.selectAll()
        self.line_edit.setFocus()
        self.slider.setVisible(False)

        #print(QApplication.focusWidget())

    def editing_finished(self) :
        #print(self.line_edit.canceled)
        if not self.line_edit.canceled :
            try :
                value = eval(self.line_edit.text())
            except :
                value = None
                print('Wrong Value')

            if isinstance(value,(float, int)) :
                self.value = value

        #print(QApplication.focusWidget())
        self.line_edit.clearFocus()
        self.line_edit.setVisible(False)
        self.slider.setVisible(True)



    @property
    def value(self):
        return self.slider.value()

    @value.setter
    def value(self, value):
        self.slider.setValue(value)

    @property
    def name(self):
        return self.slider.name

    @name.setter
    def name(self, text):
        self.slider.name = text



class Bool(QWidget) :
    value_changed = Signal(int)
    def __init__(self, parent = None, name = '') :
        super().__init__(parent)
        self.setFixedHeight(24)
        self.layout = HLayout(self)

        self.label = QLabel(name)
        self.checkbox = QCheckBox(self)

        self.layout.addWidget(self.label)
        self.layout.addStretch()
        self.layout.addWidget(self.checkbox)

        self.checked_icon = icon_path("checked")

        set_css(self,f"""
        QLabel :
            color :  rgb(220,220,220)
            font-size : 8pt
            font-weight : 500
        QCheckBox::indicator :
            width : 14px
            height : 14px
            background : 'qlineargradient(x1:0, y1:0,
                         x2:0, y2:1,stop:0 rgb(130,130,130), stop:1 rgb(100,100,100))'
            border-radius: 3px
            border-style: solid
            border-width: 1px
            border-top-color: rgb(100,100,100)
            border-left-color: rgb(100,100,100)
            border-right-color: rgb(60,60,60)
            border-bottom-color: rgb(60,60,60)
        QCheckBox::indicator:hover :
            background : 'qlineargradient(x1:0, y1:0,
                         x2:0, y2:1,stop:0 rgb(140,140,140), stop:1 rgb(110,110,110))'
        QCheckBox::indicator:checked :
            image: url({self.checked_icon})
            background : rgb(85,127,193)
        """)

        self.checkbox.stateChanged.connect(self.on_value_changed)

    def on_value_changed(self, value) :
        self.value_changed.emit(value)

    @property
    def value(self):
        return self.checkbox.isChecked()

    @value.setter
    def value(self, value):
        self.checkbox.setChecked(value)

    @property
    def name(self):
        return self.label.text()

    @name.setter
    def name(self, text):
        self.label.setText(text)

class Enum(QComboBox) :
    value_changed = Signal(str)
    def __init__(self, parent = None, name = '') :
        super().__init__()

        self.setFixedHeight(24)
        self.list_view = QListView()
        self.setView(self.list_view)
        self.icon_arrow = icon_path('arrow_down')
        self.name = name

        set_css(self,f"""
        QComboBox :
            color :  rgb(200,200,200)
            padding-left : 4px
            font-size: 8pt
            font-weight: 500
            background : 'qlineargradient(x1:0, y1:0,
                         x2:0, y2:1,stop:0 rgb(60,60,60), stop:1 rgb(45,45,45))'
            border-radius: 3px
            border-style: solid
            border-width: 1px
            border-top-color: rgb(50,50,50)
            border-left-color: rgb(50,50,50)
            border-right-color: rgb(35,35,35)
            border-bottom-color: rgb(35,35,35)
        QComboBox::drop-down :
            border: 0px
        QComboBox::down-arrow :
            border-image: url('{self.icon_arrow}')
        QComboBox QAbstractItemView :
            outline: none
            padding-bottom : 2px
            background-color : rgb(40,40,40)
        QComboBox QAbstractItemView:item :
            height : 20px
        QListView:
            color :  rgb(220,220,220)
            padding : 1px
            border : none
            font-size: 8pt
            font-weight: 500
        QListView::item :
            border : 0px
            color :  rgb(180,180,180)
            padding-left : 2px
            background : transparent
        QListView::item:selected :
            background-color : rgb(85,127,193)
            color :  rgb(220,220,220)
        """)

        self.currentIndexChanged.connect(self.on_value_changed)

    def on_value_changed(self, value) :
        self.value_changed.emit(value)

    @property
    def value(self):
        return self.currentText()

    @value.setter
    def value(self, value):
        self.setCurrentText(value)

class Button(QPushButton) :
    value_changed = Signal(str)
    def __init__(self, name = '') :
        super().__init__(name)
        self.name = name
        #self.setStyleSheet('')
        #self.setText('toto')
        #self.addItem('text')


    @property
    def value(self):
        return self.currentText()

    @value.setter
    def value(self, value):
        self.setCurrentText(value)

class String(QLineEdit) :
    value_changed = Signal(str)
    def __init__(self, name = '') :
        super().__init__()
        self.name = name
        set_css(self,"""
        QLineEdit :
            color :  rgb(220,220,220)
            background-color : rgb(80,80,80)
            selection-background-color : rgb(10,10,10)
            height : 20px
            border-style : none
            border-radius : 4px
            font-weight: 500
            font-size: 7pt
        """)

        self.textChanged.connect(self.on_value_changed)

    def on_value_changed(self, value) :
        self.value_changed.emit(value)

    @property
    def value(self):
        return self.text()

    @value.setter
    def value(self, value):
        self.setText(value)


class Color(QWidget) :
    value_changed = Signal(int)
    def __init__(self, parent = None, name = '') :
        super().__init__(parent)
        self.setFixedHeight(24)
        self.layout = HLayout(self)

        self.label = QLabel(name)
        self.layout.addWidget(self.label)
        self.layout.addStretch()

        set_css(self,f"""
        QLabel :
            color :  rgb(220,220,220)
            font-size : 8pt
            font-weight : 500
        """)

    @property
    def name(self):
        return self.label.text()

    @name.setter
    def name(self, text):
        self.label.setText(text)


    @property
    def value(self):
        return [0.0,0.0]

    @value.setter
    def value(self, value):
        return
        self.setCurrentText(value)

class Image(QLabel) :
    def __init__(self, name = '') :
        super().__init__()

    @property
    def value(self):
        image = self.pixmap().toImage()
        w, h = self.width(), self.height()
        channels = 4 if self.hasAlphaChannel() else 3
        s = image.bits().asstring(w * h * channels)
        return np.fromstring(s, dtype=np.uint8).reshape((w, h, channels))

    @value.setter
    def value(self, value):
        h, w, c = value.shape
        fmt = QImage.Format_RGBA888 if c == 4 else QImage.Format_RGB888
        self.setPixmap(QPixmap(QImage(value, w, h, c*w, fmt)))
