from PySide2.QtWidgets import QGraphicsView, QGraphicsScene
from PySide2.QtCore import Qt, QRect, QLineF, Qt
from PySide2.QtGui import QPainter, QColor, QPen
from math import sqrt

from node_editor.base_objects.node_tree import NodeTree
from node_editor.base_objects.event import MouseEvent
from node_editor.graphics.scene import Scene
from node_editor.widgets.base_widgets import *
from node_editor.widgets.popups import *
from node_editor.utils import *


class View(QGraphicsView) :
    def __init__(self, window):
        super().__init__(window)

        self.window = window
        self.settings = read_settings()
        self.setScene(Scene(self))
        self.node_tree = NodeTree(self)


    #    self.view_size = [38400,21600]
        #w, h = self.view_size
        #scene_rect = QRect(-w / 2, -h / 2, w, h)

        #self.scene().setSceneRect(scene_rect)

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        #self.setViewportUpdateMode(QGraphicsView.SmartViewportUpdate)

        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        #self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        #self.setCacheMode(QGraphicsView.CacheBackground)


        self.setRenderHint(QPainter.Antialiasing, True)
        self.setRenderHint(QPainter.TextAntialiasing, True)
        self.setRenderHint(QPainter.HighQualityAntialiasing, True)
        self.setRenderHint(QPainter.SmoothPixmapTransform, True)

        self.zoom = 1
        self.mouse_event = MouseEvent()
        self.mouse_start = QPointF()

        ## DEFINE SHORCUT
        self.node_search_key = QShortcut(QKeySequence("tab"), self)
        self.node_search_key.activated.connect(self.show_node_search)

        self.remove_node_key = QShortcut(QKeySequence(Qt.Key_Delete), self)
        self.remove_node_key.activated.connect(self.remove_node)

        self.remove_node_key = QShortcut(QKeySequence("x"), self)
        self.remove_node_key.activated.connect(self.remove_node)

        self.copy_node_key = QShortcut(QKeySequence("Ctrl+c"), self)
        self.copy_node_key.activated.connect(self.copy_node)

        self.copy_node_key = QShortcut(QKeySequence("Ctrl+v"), self)
        self.copy_node_key.activated.connect(self.paste_node)

        self.node_search = None

        self.setAcceptDrops(True)

    def dragEnterEvent(self, event) :
        event.accept()

    def dragMoveEvent(self, event) :
        event.accept()


    def dropEvent(self, event):
        files = ([u.toLocalFile()  for u in event.mimeData().urls()])
        #files = [unicode(u.toLocalFile()) for u in event.mimeData().urls()]
        for f in files:
            print(f)

    @property
    def selected_nodes(self) :
        return [n for n in self.node_tree.nodes if n.select]

    def remove_node(self) :
        for n in self.selected_nodes :
            n.remove()

    def duplicate_node(self) :
        self.copy_node()

    def copy_node(self) :
        data = yaml.dump({
        'nodes' : [n.to_dict() for n in self.selected_nodes],
        'link' :[]
        }, sort_keys = False)

        clipboard = QApplication.clipboard()
        clipboard.setText(data)

    def paste_node(self) :
        print('paste_node')
        clipboard = QApplication.clipboard().text()
        data = yaml.load(clipboard, Loader = yaml.SafeLoader)

        self.scene().clearSelection()

        for node in data['nodes'] :
            n = self.node_tree.nodes.add(node['id_name'])
            n.select = True
            n.location = node['location']
            for i, v in enumerate(node['inputs']) :
                n.inputs[i].value = v
            for i, v in enumerate(node['properties']) :
                n.properties[i].value = v

    def get_mouse_pos(self) :
        pos = QCursor().pos()
        view_pos = self.mapFromGlobal(pos)
        scene_pos = self.mapToScene(view_pos)
        return (scene_pos.x(),scene_pos.y())

    def show_node_search(self) :
        if not self.node_search :
            self.node_search = NodeSearch(self)
        self.node_search.show()
        self.node_search.search.setFocus()
        self.node_search.search.selectAll()
        #self.node_search.setFocus()

    def normalize_nodes_z(self) :
        nodes = [i for i in self.scene().items() if i.type=='node']
        z_sort_nodes = sorted(nodes, key=lambda x : x.zValue())
        for i, n in enumerate(z_sort_nodes) :
            n.setZValue(i/10-90)

    def mousePressEvent(self, event):
        self._prev_selection = self.scene().selectedItems()
        self.mouse_event = MouseEvent(event, 'PRESS')
        self.mouse_start = event.pos()


        if not self.mouse_event.MMB :
            super().mousePressEvent(event)

        self.normalize_nodes_z()
        for i in self.scene().selectedItems() :
            i.setZValue(i.zValue()+1)

        self._previous_pos = event.pos()

        if event.modifiers() == Qt.ShiftModifier :
            for n in self._prev_selection :
                n.setSelected(True)



    def mouseMoveEvent(self, event):
        self.setInteractive(True)

        if not any([self.mouse_event.LMB, self.mouse_event.RMB,self.mouse_event.MMB]) :
            super().mouseMoveEvent(event)
            return
        #    super().mouseMoveEvent(event)


        if self.mouse_event.LMB :
            super().mouseMoveEvent(event)
            """
            pass
            x1, y1 = event.pos().x(), event.pos().y()
            x2, y2 = self.mouse_start.x(), self.mouse_start.y()
            distance = sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
            if distance > 5 :
                super().mouseMoveEvent(event)
                """
        #else :
        #    super().mouseMoveEvent(event)


        if self.mouse_event.MMB :
            self.setInteractive(False)
            pos_x = (event.x() - self._previous_pos.x()) / self.zoom
            pos_y = (event.y() - self._previous_pos.y()) / self.zoom

            self.setTransformationAnchor ( QGraphicsView.NoAnchor )
            self.translate(pos_x,pos_y)

            self._previous_pos = event.pos()
            self._previous_pos = event.pos()


        # Restore Selection
        if event.modifiers() == Qt.ShiftModifier :
            for n in self._prev_selection :
                n.setSelected(True)


    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)
        self.mouse_event = MouseEvent(event, 'RELEASE')


    def wheelEvent(self, event):
        self.setInteractive(False)
        zoom = 1+ (event.angleDelta().y()/1440)

        scale = self.zoom * zoom

        if not 0.1 < scale < 2.5 : return

        self.zoom = scale

        screen_center = self.mapToScene(self.width()/2, self.height()/2)
        self.scale(zoom,zoom)
        self.centerOn(int(screen_center.x()),int(screen_center.y()))
        self.setInteractive(True)
