

class Label(QLabel) :
    def __init__(self, parent = None, text = ''):
        super().__init__(text, parent=parent)

        self.setAlignment(Qt.AlignCenter)

        self.setStyleSheet("""
        QLabel {
            background : transparent;
            color : rgb(190,190,190);
            font-weight: 600;
            font-size: 8pt;
            }
        """)

class Bool(QWidget) :
    value_changed = Signal(int)
    def __init__(self, parent = None, name = '') :
        super().__init__(parent)
        self.setFixedHeight(24)
        self.layout = QHBoxLayout(self)

        self.label = QLabel(name)
        self.checkbox = QCheckBox(self)

        self.layout.addWidget(self.label)
        self.layout.addStretch()
        self.layout.addWidget(self.checkbox)

        self.checked_icon = icon_path("checked")

        self.setStyleSheet("""
        QLabel {
            color :  rgb(220,220,220)
            }
        QCheckBox::indicator {
            width : 14px
            height : 14px
            }
        QCheckBox::indicator:checked {
            image: url(%s)
            background : rgb(85,127,193)
            }
        """%self.checked_icon)

        self.checkbox.stateChanged.connect(self.on_value_changed)

    def on_value_changed(self, value) :
        self.value_changed.emit(value)

    @property
    def value(self):
        return self.checkbox.isChecked()

    @value.setter
    def value(self, value):
        self.checkbox.setChecked(value)

    @property
    def name(self):
        return self.label.text()

    @name.setter
    def name(self, text):
        self.label.setText(text)












class Collection():
    def __init__(self):
        self._data = []

    def __setitem__(self, key, value):
        self._data.append((key, value))

    def __getitem__(self, key):
        if isinstance(key, int) :
            return self._data[key][1]
        elif isinstance(key, str) :
            return next(k[1] for k in self._data if k[0] == key)

    def __delitem__(self, key):
        if isinstance(key, int) :
            self._data.pop(key)

        elif isinstance(key, str) :
            i = next(i for i,k in enumerate(self._data) if k[0] == key)
            self._data.pop(i)

    def __len__(self):
        return len(self._data)

    def __iter__(self) :
        return (i[1] for i in self._data)

    def __repr__(self) :
        return f'{list(self.__iter__())}'

class Cube() : pass
class Sphere() : pass
class Plane() : pass

container = Collection()

container['Small Cube'] = Cube()
container['Big Cube'] = Sphere()
container['Big Cube'] = Plane()


container[0]
>>> <Cube object>

container['Big Cube']
>>> <Sphere object>

container
>>> [<Cube object>, <Sphere object>, <Plane object>]
